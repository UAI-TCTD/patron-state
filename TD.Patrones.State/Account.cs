﻿using System;

namespace TD.Patrones.State
{
    /// <summary>
    /// The 'Context' class
    /// </summary>
    public class Cuenta
    {
        State state;
        string titular;

        // Constructor
        public Cuenta(string titular)
        {
            // Todas las cuentas nuevas que se crean son de tipo [Silver]
            this.titular = titular;
            this.state = new SilverState(0.0, this);
        }

        // Properties
        public double Balance
        {
            get { return state.Balance; }
        }

        public State State
        {
            get { return state; }
            set { state = value; }
        }

        public void Deposit(double amount)
        {
            state.Deposit(amount);
            Console.WriteLine("Deposited {0:C} --- ", amount);
            Console.WriteLine(" Balance = {0:C}", this.Balance);
            Console.WriteLine(" Status  = {0}",
                this.State.GetType().Name);
            Console.WriteLine("");
        }

        public void RetiroEfectivo(double amount)
        {
            state.RetiroEfectivo(amount);
            Console.WriteLine("Retiró {0:C} --- ", amount);
            Console.WriteLine(" Balance = {0:C}", this.Balance);
            Console.WriteLine(" Status  = {0}\n",
                this.State.GetType().Name);
        }

        public void PayInterest()
        {
            state.PayInterest();
            Console.WriteLine("Interest Paid --- ");
            Console.WriteLine(" Balance = {0:C}", this.Balance);
            Console.WriteLine(" Status  = {0}\n",
                this.State.GetType().Name);
        }
    }
}
