﻿using System;

namespace TD.Patrones.State
{
    class Program
    {
        static void Main(string[] args)
        {
            // Abrimos una cuenta nueva
            var account = new Cuenta("Jim Johnson");

            // Aplicamos transacciones financieras
            account.Deposit(500.0);
            Console.WriteLine(account.State.GetType().Name);
            account.Deposit(300.0);
            Console.WriteLine(account.State.GetType().Name);
            account.Deposit(550.0);
            Console.WriteLine(account.State.GetType().Name);
            account.PayInterest();
            Console.WriteLine(account.State.GetType().Name);
            account.RetiroEfectivo(2000.00);
            Console.WriteLine(account.State.GetType().Name);
            account.RetiroEfectivo(1100.00);
            Console.WriteLine(account.State.GetType().Name);
            account.Deposit(482.50);
            Console.WriteLine(account.State.GetType().Name);
            account.Deposit(50.0);
            Console.WriteLine(account.State.GetType().Name);
            account.Deposit(100.0);
            Console.WriteLine(account.State.GetType().Name);
            account.RetiroEfectivo(20);
            Console.WriteLine(account.State.GetType().Name);
            account.Deposit(1000);
            Console.WriteLine(account.State.GetType().Name);
            account.Deposit(3000);
            
            Console.WriteLine(account.State.GetType().Name);
            account.PayInterest();
            account.RetiroEfectivo(3200+ 968.50 + 15);
            account.Deposit(30);
            Console.ReadKey();
        }
    }
}
